Deadline: vor den 28.1.2018

Created by Jonathan.

# Vormärz Script

## Status Quo

Deutschland ist geteilt, in rund 300 Staaten getrennt in Adminstartion Diplomatie und Militaer die meisten Leben in einer Monarchie und Kaiser des HDR in Wien schafft keine Einigkeit und so ist es seit mehreren hunder Jahren.

Doch die die Franzosen schaffen abwechslung und die Revolution sorgt fuer manche Deutsche hoffnung auf Freiheit und Aufklaerung andere fuerchten Terror und Gewalt der Revulolution.

* (Zeige Bild von [Demokratie] neben bild von Guillotine[Terror])

Als nun Napoleon die Macht in Frankreich an sich nimmt erobert er viele Teile Europa und setzt grosse Teile Deutschlands unter Frankreichs Kontrolle dessen Regime sorgt...

* (Bild von Europa unter Napoleon?)

dafuer das die Anzahl von Deutschen Staaten von 300 auf rund 60 schrumpfen sowie das Ideen der Aufklaerung auch in Deutschland anklang findet. 

* (Einfacher Text-bild das zeigt: "300 ----> 60")

Alsobald als die getreenten Deutschen vereint im Krieg gegen Napoleon kaempfen mussten wurde der Gedanke eines Deutschlands wieder greifbar. So hofften viele Deutsche Soldaten die in den Befreiungskriegen ihr Leben gaben wie so oft nicht nur auf ein Franzosen-Freies Deutschland sondern auch ein Deutschland wo Einikeit, Recht und Freiheit gilt.

* (Abspielen von Lied 'Was ist das Deutsche Vaterland?' und/oder 'Deutschlandlied') oder/sowie zeigen von Deutschlandflagge(Schwarz,Rot,Gold)

Nach dem Sieg ueber Napoleon muss er nurnoch zweimal auf verschiedene Insel verbannt werden und so kann 1815 endlich die Restauration beginnen:
('Animation' von Napoleon wie er auf die insel geschoben wird, dann schwimmt und wieder auf eine andere Insel geschoben wird.)

Nun versammelten sich die Sieger auf dem Wiener Kongress um die Restauration Europas zu koordinieren also: 
Liberale Kraefte in Europa entgueltig zu stoppen aber auch den Frieden und die Monarchien sicherstellen. dahinter stand vorallem der sogenannte Fuerst von Metternich.
(Zeige Bild von Fuerst von Metternich)

Geschaffen dafuer wuerde unteranderm der Deutschte Bund. Ein Buendnis Deutscher Staaten ohne Execukutive und wirklicher Macht.
* (Zeige Karte von Deutschland)

So hat der Adel wieder einmal seine Macht gesichert und der Liberale Buerger musste zuruecktreten.

Die Deutschen waren nun entauescht ueber die Wiederherstellung der alten Zustaende aber waren aber auch beruhigt das Europa die wirren des Napoleon erstmal ueberstanden haben.

Nach dem Wiener Kongresse litten viele Liberale unter Zenur, Repression und Geheimpolizei.

## Uebergang zum Vormaerz

Nun zeigte sich schon eine Erste Spaltung in dem Deutschen Buergertum und Literatur so waren mache bereit Revolutionaere Ideen wie Liberalismus und Nationalismus teils auch Sozialismus zu fordern notfalls mit Gewalt. Aber wieder andere Buerger erschuffen den sogenannten Biedermeier und damit sich selbst und Geist ins Haus und vertraute zurueckzuziehen. 

* (Bild zeigt Spaltung von Vormaerz und Biedermeier mit Pfeilen)

Ob hier bereits der Vormaerz beginnt ist umstritten aber sicher ist erstmal das 1815 der Deutschte Michel einstecken musste.

* (Bild von schlafenden oder entaeuschten Deutschen Michel)

## Vormaerz Geschichte

Deutschland nach Napoleon war nun bestimmt durch Oestereich und Preussen jene konservativen Monarchien waren bedrohung durch Liberale und Demokraten bewusst und liesen jene Verfolgen. 

* (Bild vom Deutschen Michel im vogelkaefig)

So musste der Verfasser vom Deutschlandlied Hoffmann von Fallersleben aus Angst vor represallien auf damalst britische Helgoland fliehen musste.

* (Karten-Bild von Helgoland oder von Hoffmann von Fallersleben)

Die Jahre vergingen und Unruhen und Repressalien befanden sich im expontiellen Duell.
Sobald als in Frankreich erneut eine Revolution ausbrach bruchen deswegen erneut Unruhnen aber auch lecihte Reformen im Europa und Deutschland aus. 

* (Bild von Frankreichs-Konig-Flagge zu Frankreich rebublik)

Deutsche Demostrationen spitzten sich auf den Hambacher Fest zu in der Rheinpfalz auf dem Hambacher Schloss.
Burschenschaften die fast im ganzen Deutschen Bund verboten wurden sind versammelten sich zu dem
Höhepunkt der bürgerlicher Opposition zu Beginn des Vormärz um unteranderem 'Ehre, Freiheit, Vaterland' anzustimmen.

* (Bild vom Hambacher Fest)

Nun wo der Vormärz in vollen zuegen steht bluehten Literatur und Kunst und der Biedermeier wird kurz und knapp in seiner Popularitaet uebertroffen.

* (Zeige Bild von brennende Krone)

Auf den Hambacher Fest folgte der Frankfurter Wachensturm wobei rund 100 studenten vergebens versuchten auf mit einen Angriff auf die Hauptwache am Frankfurt am Main eine Deutschlandweite Revolution anzuzetteln.

* (Zeige 'Animation' von  gescheiterten Frankfurter Wachensturm: Figuren versuchen Haus einzunehmen und man hoert schuesse und die Angreifer fliehen.)

Doch all diese Versuchen waren vergebens und es bedarfte 1848 eine Erneute Revultion in Frankreich um auch in Deutschland eine Revultion anzuzuenden. 

* (Panisches-Ahnungsloses umherwuehlen in Frankreich-Material bis dann die Frankreich-Republik-Flagge rausgezogen wird.)

So began die Maerzrevultion in Deutschland im Jahr 1848 und das Wirrwarr der Revolution began erneut zu eskalieren.

* (Deutschlandflagge Brennend)

## Vormaerz Literatur

So hat der Vormaerz uns vorallem viel Literatur, Kunst und Musik vereerbt.
( Text: Literatur, Kunst und Musik)


### Themen der Epoche 

Da im Vormärz die Kunst und die Politik vermischt wurden, war die Politik und die Unzufriedenheit über die sozialen Umstände von zentraler Bedeutung. Die Autoren riefen zur Rebellion auf und kritisierten den Adel und die Ständegesellschaft. Aus diesem Grund wurden viele Autoren des Vormärz mit einem Publikationsverbot bestraft, was diese jedoch nicht davon abhielt, weiter für ihre Ideen zu kämpfen.

* (Bild von Deutschlandflagge aber durchgestrichen)

Da die Schriftsteller nun für eine breite Öffentlichkeit schrieben, mussten sie diese natürlich so direkt wie möglich ansprechen. Daher wurden oftmals Leute aus dem untersten Stand zum Helden gemacht und es wurde zur Einheit und zum gemeinsamen Kampf gegen die Obrigkeiten aufgerufen. Auch die Frauen begannen sich in dieser Epoche zu emanzipieren und für ihre Rechte zu kämpfen.

* (Bild zeigt Bauer in mit Schwert ready to fight)

### Formalheiten

Da die Literatur nicht mehr nur Kunst war sondern auch Politik, veränderten sich auch die formalen Begebenheiten. Die Publizistik erlebte ihren bisherigen Höhepunkt und es wurde auch häufig mit Flugblättern gearbeitet. Die Sprache veränderte sich ebenfalls stark, denn um das einfache Volk anzusprechen, begann man in Dialekt zu schreiben. Damit konnten auch die sozialen Unterschiede zwischen den Armen und den Reichen formal aufgezeigt werden. Literatur dient also dazu jeden zu erlauben an der Revolution teilzunehmen und so jene anzustacheln

* (Bild zeigt mehrere arme menschen sind sauer und sprechen verschiedene Deutschte Dialekte der einzige Adelige steht abseit und sagt "Demokratie ist die Diktatur der Dummen")

Viele sind sich einig das wohl repräsentativste Werk dieser Epoche ist „Woycek“ von Georg Büchner. Dieses offene Drama zeigt die sozialen Missstände und die Unterschiede zwischen Arm und Reich inhaltlich wie formal äusserst treffend.

* (Bild von Georg Büchner)

### Autoren

* Es folgt eine Humoristisch-Dramatische Abschann von wichtigen Autoren und ihren Portraits untermalt von Dramatischer Musik:
* (Text: 1830 - 1848 #NeverForget) 
* (Bilder von Heinrich Heine, Karl Guzkow, Ludolf Wienbarg, Ludwig Börne, Theodor Mundt)



